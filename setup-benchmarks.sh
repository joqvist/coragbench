#!/bin/bash

# Set up systems used in default benchmark run.

if [ -d "systems_10" ]; then
  rm -r systems_10
fi
export QC_SYSTEMS="systems_10"
./qualitas.sh ant 1.8.4
./qualitas.sh antlr 4.0
./qualitas.sh aspectj 1.6.9 -encoding iso-8859-1
./qualitas.sh argouml 0.34 -encoding iso-8859-1
./qualitas.sh azureus 4.8.1.2 -encoding iso-8859-1
./qualitas.sh castor 1.3.1 -encoding iso-8859-1
./qualitas.sh cayenne 3.0.1 -encoding iso-8859-1
./qualitas.sh checkstyle 5.1 -encoding iso-8859-1
./qualitas.sh cobertura 1.9.4.1 -encoding iso-8859-1

if [ -d "systems_5" ]; then
  rm -r systems_5
fi
mkdir systems_5
cp "systems_10/ant-1.8.4" systems_5
cp "systems_10/antlr-4.0" systems_5
cp "systems_10/aspectj-1.6.9" systems_5
cp "systems_10/argouml-0.34" systems_5
cp "systems_10/azureus-4.8.1.2" systems_5

if [ -d "systems_1" ]; then
  rm -r systems_1
fi
mkdir systems_1
cp "systems_10/ant-1.8.4" systems_1
