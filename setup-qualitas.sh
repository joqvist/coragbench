#!/bin/bash

interrupted()
{
    exit $?
}

trap interrupted SIGINT

# Exoportal has .java files that use enum as an identifier. (Java 4)
# despite being tagged as JDK 1.5 in Qualitas Corpus.

# ./qualitas.sh exoportal v1.0.2

# Derby, Hadoop, and Megamek cause StackOverflowErorrs in ExtendJ so we must skip them:
# ./qualitas.sh derby 10.6.1.0
# ./qualitas.sh hadoop 1.0.0
# ./qualitas.sh megamek 0.35.18

# Eclipse and Hibernate fail to parse correctly with ExtendJ (NPE).
# ./qualitas.sh eclipse_SDK 4.3
# ./qualitas.sh hibernate 4.2.2

# gt2-2.7-M3 causes a null variable declaration in ExtendJ.
# ./qualitas.sh gt2 2.7-M3

# The JRE system is missing in Qualitas.
# ./qualitas.sh jre 1.6.0

# The NetBeans build takes abnormally long time with ExtendJ.
# ./qualitas.sh netbeans 6.9.1

# AOI takes too much memory to build with ExtendJ.
#./qualitas.sh aoi 2.8.1 -encoding iso-8859-1

./qualitas.sh ant 1.8.4

./qualitas.sh antlr 4.0

./qualitas.sh aspectj 1.6.9 -encoding iso-8859-1

./qualitas.sh argouml 0.34 -encoding iso-8859-1
# (JDK 1.3) ./qualitas.sh axion 1.0-M2
./qualitas.sh azureus 4.8.1.2 -encoding iso-8859-1
# (JDK 1.4) ./qualitas.sh batik 1.7
# (JDK 1.4) ./qualitas.sh c_jdbc 2.0.2
./qualitas.sh castor 1.3.1 -encoding iso-8859-1
./qualitas.sh cayenne 3.0.1 -encoding iso-8859-1
./qualitas.sh checkstyle 5.1 -encoding iso-8859-1
./qualitas.sh cobertura 1.9.4.1 -encoding iso-8859-1
# (JDK 1.4) ./qualitas.sh collections 3.2.
# (JDK 1.4) ./qualitas.sh colt 1.2.0
# (JDK 1.4) ./qualitas.sh columba 1.0
./qualitas.sh compiere 330 -encoding iso-8859-1
# (JDK 1.4) ./qualitas.sh displaytag 1.2
# (JDK 1.4) ./qualitas.sh drawswf 1.2.9
./qualitas.sh drjava stable-20100913-r5387
# (JDK 1.4) ./qualitas.sh emma 2.0.5312
./qualitas.sh findbugs 1.3.9
# (JDK 1.4) ./qualitas.sh fitjava 1.1
./qualitas.sh fitlibraryforfitnesse 20100806
./qualitas.sh freecol 0.10.7
./qualitas.sh freecs 1.3.20100406 -encoding iso-8859-1
# (JDK 1.4) ./qualitas.sh freemind 0.9.0
# (JDK 1.4) ./qualitas.sh galleon 2.3.0
# (JDK 1.4) ./qualitas.sh ganttproject 2.0.9
./qualitas.sh heritrix 1.14.4
./qualitas.sh hsqldb 2.0.0
./qualitas.sh htmlunit 2.8
./qualitas.sh informa 0.7.0-alpha2 -encoding iso-8859-1
./qualitas.sh ireport 3.7.5
./qualitas.sh itext 5.0.3
# (JDK 1.4) ./qualitas.sh ivatagroupware 0.11.3
./qualitas.sh jFin_DateMath R1.0.1 -encoding iso-8859-1
# (JDK 1.4) ./qualitas.sh jag 6.1
# (uses enum as identifier) ./qualitas.sh james 2.2.0
# (JDK 1.4) ./qualitas.sh jasml 0.10
./qualitas.sh jasperreports 3.7.3
./qualitas.sh javacc 5.0
./qualitas.sh jboss 5.1.0
./qualitas.sh jchempaint 3.0.1
./qualitas.sh jedit 4.3.2
./qualitas.sh jena 2.6.3
./qualitas.sh jext 5.0
# (JDK 1.4) ./qualitas.sh jfreechart 1.0.13
# (JDK 1.4) ./qualitas.sh jgraph 5.13.0.0
./qualitas.sh jgraphpad 5.10.0.2
./qualitas.sh jgrapht 0.8.1
./qualitas.sh jgroups 2.10.0
./qualitas.sh jhotdraw 7.5.1
./qualitas.sh jmeter 2.9
# (JDK 1.4) ./qualitas.sh jmoney 0.4.4
# (JDK 1.4) ./qualitas.sh joggplayer 1.1.4s
# (JDK 1.4) ./qualitas.sh jparse 0.96
./qualitas.sh jpf 1.5.1
# (JDK 1.4) ./qualitas.sh jrat 0.6
# (JDK 1.4) ./qualitas.sh jrefactory 2.9.19
./qualitas.sh jruby 1.5.2
./qualitas.sh jsXe 04_beta
./qualitas.sh jspwiki 2.8.4
./qualitas.sh jstock 1.0.7c
# (JDK 1.4) ./qualitas.sh jtopen 7.1
./qualitas.sh jung 2.0.1
./qualitas.sh junit 4.11
# (JDK 1.4) ./qualitas.sh log4j 1.2.16
./qualitas.sh lucene 4.3.0
./qualitas.sh marauroa 3.8.1
./qualitas.sh maven 3.0
# (JDK 1.4) ./qualitas.sh mvnforum 1.2.2-ga
./qualitas.sh myfaces_core 2.0.2
./qualitas.sh nakedobjects 4.0.0
./qualitas.sh nekohtml 1.9.14
# (JDK 1.2) ./qualitas.sh openjms 0.7.7-beta-1
# (JDK 1.4) ./qualitas.sh oscache 2.4.1
./qualitas.sh picocontainer 2.10.2
./qualitas.sh pmd 4.2.5
./qualitas.sh poi 3.6
./qualitas.sh pooka 3.0-080505
# (JDK 1.4) ./qualitas.sh proguard 4.5.1
./qualitas.sh quartz 1.8.3
# (JDK 1.4) ./qualitas.sh quickserver 1.4.7
# (JDK 1.4) ./qualitas.sh quilt 0.6-a-5
./qualitas.sh roller 4.0.1
./qualitas.sh rssowl 2.0.5
./qualitas.sh sablecc 3.2
# (JDK 1.4) ./qualitas.sh sandmark 3.4
./qualitas.sh springframework 3.0.5
./qualitas.sh squirrel_sql 3.1.2
./qualitas.sh struts 2.2.1
./qualitas.sh sunflow 0.07.2
./qualitas.sh tapestry 5.1.0.5
./qualitas.sh tomcat 7.0.2
./qualitas.sh trove 2.1.0
# (JDK 1.4) ./qualitas.sh velocity 1.6.4
./qualitas.sh wct 1.5.2
# (JDK 1.2) ./qualitas.sh webmail 0.7.10
./qualitas.sh weka 3.7.9
# (JDK 1.3) ./qualitas.sh xalan 2.7.1
# (JDK 1.3) ./qualitas.sh xerces 2.10.0
# (JDK 1.2) ./qualitas.sh xmojo 5.0.0
