#!/usr/bin/env groovy

import groovy.io.FileType
import groovy.util.FileNameFinder

def cli = new CliBuilder(usage: 'qcc-setup.groovy [-c dir] [-d dir] <system1 system2...>')

cli.with {
  c longOpt: 'corpus', args:1, argName:'dir', 'Qualitas.class corpus directory'
  d longOpt: 'dest', args:1, argName:'dir', 'System configuration output directory'
  e longOpt: 'encoding', args:1, argName:'encoding', 'Set source encoding'
}

def options = cli.parse(args)
if (options.arguments().isEmpty()) {
  cli.usage()
  return
}

def outDir = new File(options.d ?: '.')
if (!outDir.isDirectory()) {
  outDir.mkdir()
}

def qcc = new File(options.c ?: '.')
options.arguments().each { system ->
  def systemDir = new File(qcc, system)
  if (!systemDir.isDirectory()) {
    def zipfile = new File(qcc, system + '.zip')
    if (!zipfile.isFile()) {
      println "ERROR: System Zip file not found: ${zipfile.absolutePath}."
      if (!options.c) {
        println 'Try specifying the corpus directory (-c dir).'
      }
      return
    }
    def process = new ProcessBuilder(
        'unzip', zipfile.absolutePath, '-d', qcc.absolutePath)
        .redirectErrorStream(true).start()
    process.inputStream.eachLine { line -> println line }
  }
  def cpFile = new File(systemDir, '.classpath')
  if (!cpFile.isFile()) {
    println "ERROR: System .classpath file not found for ${system}."
    return
  }
  def classpath = new XmlParser().parse(cpFile)
  def lib = classpath.classpathentry.grep { entry -> entry.attribute('kind') == 'lib' }
      .collect { entry ->
        def path = entry.attribute('path')
        new File(systemDir, path).absolutePath
      }
  def con = classpath.classpathentry.grep { entry -> entry.attribute('kind') == 'con' }
      .collect { entry ->
        def path = entry.attribute('path')
        switch (path) {
        case 'org.eclipse.jdt.junit.JUNIT_CONTAINER/4':
          return [ 'lib/junit-4.12.jar', 'lib/hamcrest-core-1.3.jar' ]
        case 'org.eclipse.jdt.launching.JRE_CONTAINER':
          return [ ]
        default:
          println "WARNING: Unknown classpath container: ${path}."
          return [ ]
        }
      }
      .flatten()
  def sources = classpath.classpathentry.grep { entry -> entry.attribute('kind') == 'src' }
      .collect { entry ->
        def path = entry.attribute('path') ?: ''
        def excludes = (entry.attribute('excluding')?.split('\\|')) ?: []
        def dir = path.isEmpty() ? systemDir : new File(systemDir, path)
        def sources = new FileNameFinder().getFileNames(dir.absolutePath,
            '**/*.java',
            excludes.join(' '))
        sources
      }
      .flatten()
      .unique()
  if (sources.isEmpty()) {
    println "ERROR: no sources found for ${system}"
    return
  }
  def argfile = new File(outDir, system)
  argfile.withWriter { writer ->
    writer.writeLine '-d'
    writer.writeLine 'tmp'
    def libs = ((lib + con).join(':'))
    if (libs) {
      writer.writeLine '-classpath'
      writer.writeLine libs
    }
    sources.each { writer.writeLine it }
  }

  if (true) {
    println "Compiling ${system} with javac..."
    def tstart = System.currentTimeMillis()
    def encoding = options.e ?: 'utf-8'
    def process = new ProcessBuilder('javac', '-encoding', encoding, "@${argfile.path}")
        .redirectErrorStream(true)
        .redirectOutput(ProcessBuilder.Redirect.to(new File("logs/${system}.javac.log")))
        .start()
    process.waitFor()
    def result = process.exitValue()
    def time = System.currentTimeMillis() - tstart
    println "    result: ${result} ${time/1000.0}s"
  }

  if (true) {
    println "Compiling ${system} with extendj..."
    def tstart = System.currentTimeMillis()
    def process = new ProcessBuilder('java', '-jar', '/home/jesper/git/extendj/extendj.jar', "@${argfile.path}")
        .redirectErrorStream(true)
        .redirectOutput(ProcessBuilder.Redirect.to(new File("logs/${system}.extendj.log")))
        .start()
    process.waitFor()
    def result = process.exitValue()
    def time = System.currentTimeMillis() - tstart
    println "    result: ${result} ${time/1000.0}s"
  }
}
