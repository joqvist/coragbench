#!/bin/bash

# This script builds the classpath for compiling a Qualitas Corpus program.

set -e

interrupted()
{
    exit $?
}

trap interrupted SIGINT

if [ $# -lt "2" ]; then
    echo "Usage: $0 SHORT_NAME SYSTEM_NAME"
	echo "    SHORT_NAME    The short name of a Qualitas system"
	echo "    VERSION       The version name of the Qualitas system"
    exit 1
fi

# Path to the Qualitas Corpus:
QC_PATH="$HOME/qualitas"

if [ ! -d "$QC_PATH" ]; then
	echo "Error: Qualitas Corpus path is not a directory: ${QC_PATH}"
	exit 1
fi

SHORT_NAME="$1"
SYSTEM_NAME="${SHORT_NAME}-$2"

SYSTEM_PATH="${QC_PATH}/Systems/${SHORT_NAME}/${SYSTEM_NAME}"

echo "## Setting up ${SYSTEM_NAME}"

if [ ! -d "$SYSTEM_PATH" ]; then
	echo "Error: system directory not found: ${SYSTEM_PATH}"
	exit 1
fi

# Locate library Jars in the system:
JARS=$(find "$SYSTEM_PATH" -name '*.jar')
CLASSPATH="${JARS//$'\n'/:}"

QC_SYSTEMS="${QC_SYSTEMS:-systems}"
if [ ! -d "$QC_SYSTEMS" ]; then
	mkdir "$QC_SYSTEMS"
fi
ARGFILE="$QC_SYSTEMS/${SYSTEM_NAME}"

# Generate the compile arguments file:
echo "-d" > "$ARGFILE"
echo "tmp" >> "$ARGFILE"
echo "-classpath" >> "$ARGFILE"
echo "\"${CLASSPATH}\"" >> "$ARGFILE"
# Use the Qualitas metadata to find source files to be compiled:
awk -F'\t' '/^[^#]/ {split($3,a,","); for(i in a) print a[i]}' \
	"${SYSTEM_PATH}/metadata/contents.csv" \
	| uniq \
	| while read sf
do
	echo "\"${SYSTEM_PATH}/src/${sf}\"" >> "$ARGFILE"
done

./test.sh "$SYSTEM_NAME" ${@:3}
