#!/bin/bash

# Path to the Qualitas Corpus:
QC_PATH="$HOME/qualitas"

if [ ! -d "$QC_PATH" ]; then
	echo "Error: Qualitas Corpus path is not a directory: ${QC_PATH}"
	exit 1
fi

awk -F'\t' '/^[^#]/ {printf("%s,%s,%s\n", $5, $17, $18)}' \
	"${QC_PATH}/metadata/summary.csv" > qcsummary.csv

