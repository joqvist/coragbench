/*
 * Copyright (c) 2017, The JastAdd Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *   3. The name of the author may not be used to endorse or promote
 *      products derived from this software without specific prior
 *      written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.drast.benchmark;

import drast.model.SourceFile;

import java.util.Collection;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;
import java.util.Random;

import org.jastadd.drast.Benchmark;

public class FindErrorsSeq extends BenchmarkTask {
  private final Collection<SourceFile> files;

  public FindErrorsSeq(Collection<SourceFile> files, int seed) {
    this.files = Benchmark.shuffle(files, new Random(seed));
  }

  @Override public Thread concurrentTask(CountDownLatch startLatch,
      CountDownLatch taskLatch) {
    return new Thread(() -> {
      try {
        startLatch.countDown();
        taskLatch.await();
        long start1 = System.currentTimeMillis();
        int errors = 0;
        int warnings = 0;
        for (SourceFile file : files) {
          errors += file.root.errors().size();
          warnings += file.root.warnings().size();
        }
        long time = System.currentTimeMillis() - start1;
        results.add(new Result("errors", errors));
        results.add(new Result("warnings", warnings));
        results.add(new Result("problemTime", time));
      } catch (InterruptedException e1) {
        throw new Error(e1);
      }
    });
  }

  @Override public Thread sequentialTask(CountDownLatch startLatch,
      CountDownLatch taskLatch, ReentrantLock lock) {
    return new Thread(() -> {
      try {
        startLatch.countDown();
        taskLatch.await();
        long start1 = System.currentTimeMillis();
        int errors = 0;
        int warnings = 0;
        // Sequential evaluation of all semantic errors.
        synchronized (lock) {
          for (SourceFile file : files) {
            errors += file.root.errors().size();
            warnings += file.root.warnings().size();
          }
        }
        long time = System.currentTimeMillis() - start1;
        results.add(new Result("errors", errors));
        results.add(new Result("warnings", warnings));
        results.add(new Result("problemTime", time));
      } catch (InterruptedException e1) {
        throw new Error(e1);
      }
    });
  }
}
