/*
 * Copyright (c) 2017, The JastAdd Team
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 *   1. Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *
 *   2. Redistributions in binary form must reproduce the above
 *      copyright notice, this list of conditions and the following
 *      disclaimer in the documentation and/or other materials provided
 *      with the distribution.
 *
 *   3. The name of the author may not be used to endorse or promote
 *      products derived from this software without specific prior
 *      written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
 * IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd.drast.benchmark;

import drast.model.SourceFile;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;

import org.jastadd.drast.Benchmark;

public class NodeMapper<T> extends BenchmarkTask {
  /**
   * Number of nodes to send to the consumer.
   * This is the number of nodes used in the
   * variable declaration and method type benchmark tasks.
   */
  private static final int NUM_NODES = 500;
  private final List<T> nodes;
  private final Consumer<T> consumer;

  public interface NodeFinder<U> {
    List<U> findNodes();
  }

  public NodeMapper(NodeFinder<T> nodeFinder, Random random, Consumer<T> consumer) {
    long start = System.currentTimeMillis();
    List<T> foundNodes = nodeFinder.findNodes();
    long time = System.currentTimeMillis() - start;
    this.nodes = Benchmark.shuffle(foundNodes, random);
    results.add(new Result("nodeTime", time));
    this.consumer = consumer;
  }

  @Override public Thread concurrentTask(CountDownLatch startLatch,
      CountDownLatch taskLatch) {
    return new Thread(() -> {
      try {
        startLatch.countDown();
        taskLatch.await();
        long start = System.currentTimeMillis();
        Iterator<T> iterator = nodes.iterator();
        int num = 0;
        for (int i = 0; i < NUM_NODES && iterator.hasNext(); ++i) {
          consumer.accept(iterator.next());
          num += 1;
        }
        long time = System.currentTimeMillis() - start;
        results.add(new Result("numNodes", nodes.size()));
        results.add(new Result("attributeCount", num));
        results.add(new Result("attributeTime", time));
      } catch (InterruptedException e) {
        throw new Error(e);
      }
    });
  }

  @Override public Thread sequentialTask(CountDownLatch startLatch,
      CountDownLatch taskLatch, ReentrantLock lock) {
    return new Thread(() -> {
      try {
        startLatch.countDown();
        taskLatch.await();
        long start = System.currentTimeMillis();
        Iterator<T> iterator = nodes.iterator();
        int num = 0;
        for (int i = 0; i < NUM_NODES && iterator.hasNext(); ++i) {
          synchronized (lock) {
            consumer.accept(iterator.next());
          }
          num += 1;
        }
        long time = System.currentTimeMillis() - start;
        results.add(new Result("numNodes", nodes.size()));
        results.add(new Result("attributeCount", num));
        results.add(new Result("attributeTime", time));
      } catch (InterruptedException e) {
        throw new Error(e);
      }
    });
  }
}
