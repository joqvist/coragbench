package org.extendj.ast;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;

public class SynchronizedConcurrentMap<K, V> implements ConcurrentMap<K, V> {
  HashMap<K, V> map = new HashMap<>();

  @Override public synchronized V putIfAbsent(K key, V value) {
    return map.putIfAbsent(key, value);
  }

  @Override public synchronized V get(Object key) {
    return map.get(key);
  }

  @Override public synchronized boolean containsKey(Object key) {
    return map.containsKey(key);
  }

  @Override public synchronized boolean replace(K key, V oldValue, V newValue) {
    return map.replace(key, oldValue, newValue);
  }

  @Override public boolean containsValue(Object value) {
    throw new UnsupportedOperationException();
  }

  @Override public int size() {
    throw new UnsupportedOperationException();
  }

  @Override public boolean isEmpty() {
    throw new UnsupportedOperationException();
  }

  @Override public V put(K key, V value) {
    throw new UnsupportedOperationException();
  }

  @Override public V remove(Object key) {
    throw new UnsupportedOperationException();
  }

  @Override public void putAll(Map<? extends K, ? extends V> m) {
    throw new UnsupportedOperationException();
  }

  @Override public void clear() {
    throw new UnsupportedOperationException();
  }

  @Override public Set<K> keySet() {
    throw new UnsupportedOperationException();
  }

  @Override public Collection<V> values() {
    throw new UnsupportedOperationException();
  }

  @Override public Set<Entry<K, V>> entrySet() {
    throw new UnsupportedOperationException();
  }

  @Override public boolean remove(Object key, Object value) {
    throw new UnsupportedOperationException();
  }

  @Override public V replace(K key, V value) {
    throw new UnsupportedOperationException();
  }
}
