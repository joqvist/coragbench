package drast.starter;

import drast.Log;
import drast.model.SourceFile;
import org.extendj.ast.ASTNode;
import org.extendj.ast.CompilationUnit;
import org.extendj.ast.FileClassSource;
import org.extendj.ast.Options;
import org.extendj.ast.Program;
import org.extendj.ast.SourceFilePath;
import org.extendj.parser.JavaParser;
import org.extendj.scanner.JavaScanner;

import java.io.File;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Consumer;

/**
 * Provides an AST by running the ExtendJ parser.
 */
public class ASTProvider {
  /**
   * Runs the target compiler.
   */
  public static boolean parseAst(String text, Consumer<Object> rootConsumer) {
    long start = System.currentTimeMillis();
    try (StringReader reader = new StringReader(text)){
      JavaScanner scanner = new JavaScanner(reader);
      JavaParser parser = new JavaParser();
      ASTNode.resetState();
      Program program = new Program();
      CompilationUnit cu = (CompilationUnit) parser.parse(scanner);
      cu.setFromSource(true);
      cu.setClassSource(new FileClassSource(new SourceFilePath("inputfile"), "inputfile"));
      program.addCompilationUnit(cu);
      Log.info("Parsing finished after %d ms", (System.currentTimeMillis() - start));
      rootConsumer.accept(program);
      return true;
    } catch (Throwable e) {
      e.printStackTrace();
      Log.error(e.getMessage());
      return false;
    }
  }

  public static boolean parseAst(Collection<String> args,
      Consumer<Collection<SourceFile>> rootConsumer) {
    long start = System.currentTimeMillis();
    ASTNode.resetState();
    Program program = new Program();
    program.initBytecodeReader(Program.defaultBytecodeReader());
    program.initJavaParser(Program.defaultJavaParser());

    parseOptions(program, args);

    List<File> files = new ArrayList<>();
    for (String arg : program.options().files()) {
      File file = new File(arg);
      if (!file.isFile()) {
        System.err.println("Error: neither a valid option nor a filename: " + file);
      } else {
        files.add(new File(arg));
      }
    }

    Collection<SourceFile> sourceFiles = new ArrayList<>();
    try {
      // Parse source files:
      for (File file : files) {
        CompilationUnit cu = program.addSourceFile(file.getAbsolutePath());
        sourceFiles.add(new SourceFile(file, cu));
      }
      Log.info("Parsing finished after %d ms", (System.currentTimeMillis() - start));
      rootConsumer.accept(sourceFiles);
      return true;
    } catch (Throwable e) {
      e.printStackTrace();
      Log.error(e.getMessage());
      return false;
    }
  }

  private static void parseOptions(Program program, Collection<String> args) {
    initOptions(program);

    String[] argArray = new String[args.size()];
    args.toArray(argArray);
    program.options().addOptions(argArray);
  }

  private static void initOptions(Program program) {
    Options options = program.options();
    options.initOptions();
    options.addKeyOption("-version");
    options.addKeyOption("-print");
    options.addKeyOption("-g");
    options.addKeyOption("-g:none");
    options.addKeyOption("-g:lines,vars,source");
    options.addKeyOption("-nowarn");
    options.addKeyOption("-verbose");
    options.addKeyOption("-deprecation");
    options.addKeyValueOption("-classpath");
    options.addKeyValueOption("-cp");
    options.addKeyValueOption("-sourcepath");
    options.addKeyValueOption("-bootclasspath");
    options.addKeyValueOption("-extdirs");
    options.addKeyValueOption("-d");
    options.addKeyValueOption("-encoding");
    options.addKeyValueOption("-source");
    options.addKeyValueOption("-target");
    options.addKeyOption("-help");
    options.addKeyOption("-O");
    options.addKeyOption("-J-Xmx128M");
    options.addKeyOption("-recover");
    options.addKeyOption("-XprettyPrint");
    options.addKeyOption("-XdumpTree");

    // Non-javac options.
    options.addKeyOption("-profile"); // Output profiling information.
    options.addKeyOption("-debug"); // Extra debug checks and information.
  }
}

