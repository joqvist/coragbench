package drast.views.gui.dialogs;

import drast.model.DrASTSettings;
import drast.views.gui.GUIData;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class CompilerArgumentsDialog extends Stage {
  private final ListView<String> arguments = new ListView<>();

  public CompilerArgumentsDialog(GUIData mon) {
    String prevArgs = DrASTSettings.get(DrASTSettings.PREV_ARGS, "");
    arguments.getItems().addAll(argsToArray(prevArgs));

    VBox content = new VBox();
    content.setSpacing(10);
    content.setPadding(new Insets(10));
    Label label = new Label("Compiler arguments:");
    HBox buttons1 = new HBox();
    buttons1.setAlignment(Pos.BOTTOM_RIGHT);
    buttons1.setSpacing(10);
    HBox buttons2 = new HBox();
    buttons2.setAlignment(Pos.BOTTOM_RIGHT);
    buttons2.setSpacing(10);
    Button setAllArguments = new Button("Set all arguments");
    setAllArguments.setOnAction(event -> {
      TextInputDialog dialog = new TextInputDialog();
      dialog.setTitle("Set all Arguments");
      dialog.setHeaderText("Replace all command-line arguments");
      dialog.setContentText("Arguments:");
      Optional<String> result = dialog.showAndWait();
      if (result.isPresent()) {
        arguments.getItems().setAll(argsToArray(result.get()));
      }
    });
    Button addArgumentFile = new Button("Load argument file");
    addArgumentFile.setOnAction(event -> {
      FileChooser dialog = new FileChooser();
      File initialDir = null;
      int selected = arguments.getSelectionModel().getSelectedIndex();
      if (selected >= 0 && selected < arguments.getItems().size()) {
        File file = new File(arguments.getItems().get(selected));
        if (file.isFile()) {
          initialDir = file.getParentFile();
        }
      }
      if (initialDir == null || !initialDir.isDirectory()) {
        initialDir = new File(System.getProperty("user.dir"));
      }
      if (initialDir.isDirectory()) {
        dialog.setInitialDirectory(initialDir);
      }
      File result = dialog.showOpenDialog(this);
      if (result != null) {
        try (Scanner in = new Scanner(new FileInputStream(result))) {
          arguments.getItems().clear();
          while (in.hasNextLine()) {
            String line = in.nextLine();
            if (line.startsWith("\"") && line.endsWith("\"")) {
              line = line.substring(1, line.length() - 1);
            }
            addAfterSelectedArgument(line);
          }
        } catch (IOException ignored) {
        }
      }
    });
    Button addArguments = new Button("Add argument");
    addArguments.setOnAction(event -> {
      TextInputDialog dialog = new TextInputDialog();
      dialog.setTitle("Add Argument");
      dialog.setHeaderText("Add a single command-line argument");
      dialog.setContentText("Argument:");
      Optional<String> result = dialog.showAndWait();
      if (result.isPresent()) {
        addAfterSelectedArgument(result.get());
      }
    });
    Button addFiles = new Button("Add files");
    addFiles.setOnAction(event -> {
      FileChooser dialog = new FileChooser();
      File initialDir = null;
      int selected = arguments.getSelectionModel().getSelectedIndex();
      if (selected >= 0 && selected < arguments.getItems().size()) {
        File file = new File(arguments.getItems().get(selected));
        if (file.isFile()) {
          initialDir = file.getParentFile();
        }
      }
      if (initialDir == null || !initialDir.isDirectory()) {
        initialDir = new File(System.getProperty("user.dir"));
      }
      if (initialDir.isDirectory()) {
        dialog.setInitialDirectory(initialDir);
      }
      List<File> result = dialog.showOpenMultipleDialog(this);
      if (result != null) {
        for (File file : result) {
          addAfterSelectedArgument(file.getPath());
        }
      }
    });
    Button edit = new Button("Edit");
    edit.setOnAction(event -> {
      int selected = arguments.getSelectionModel().getSelectedIndex();
      if (selected >= 0 && selected < arguments.getItems().size()) {
        String argument = arguments.getItems().get(selected);
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle("Edit Argument");
        dialog.setHeaderText("Edit a command-line argument");
        dialog.setContentText("Argument:");
        dialog.getEditor().setText(argument);
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent()) {
          arguments.getItems().set(selected, result.get());
        }
      }
    });
    Button remove = new Button("Remove");
    remove.setOnAction(event -> {
      int selected = arguments.getSelectionModel().getSelectedIndex();
      if (selected >= 0 && selected < arguments.getItems().size()) {
        arguments.getItems().remove(selected);
      }
    });
    Button up = new Button("Up");
    up.setOnAction(event -> {
      int selected = arguments.getSelectionModel().getSelectedIndex();
      if (selected > 0 && selected < arguments.getItems().size()) {
        String arg = arguments.getItems().remove(selected);
        arguments.getItems().add(selected - 1, arg);
        arguments.getSelectionModel().select(selected - 1);
      }
    });
    Button down = new Button("Down");
    down.setOnAction(event -> {
      int selected = arguments.getSelectionModel().getSelectedIndex();
      if (selected >= 0 && selected + 1 < arguments.getItems().size()) {
        String arg = arguments.getItems().remove(selected);
        arguments.getItems().add(selected + 1, arg);
        arguments.getSelectionModel().select(selected + 1);
      }
    });
    Button cancel = new Button("Cancel");
    cancel.setOnAction(e -> hide());
    Button ok = new Button("Ok");
    ok.setDefaultButton(true);
    ok.setOnAction(event -> {
      mon.getController().runCompiler(arguments.getItems());
      hide();
    });
    buttons1.getChildren().addAll(addArgumentFile, setAllArguments, addArguments, addFiles);
    buttons2.getChildren().addAll(edit, remove, up, down, cancel, ok);
    content.getChildren().addAll(label, arguments, buttons1, buttons2);
    setScene(new Scene(content));
    addEventFilter(KeyEvent.KEY_PRESSED, e -> {
      if (e.getCode() == KeyCode.ESCAPE) {
        e.consume();
        hide();
      }
    });
    setTitle("Compiler Arguments");
    initModality(Modality.NONE);
    setTitle("Open compiler...");
  }

  private String[] argsToArray(String arguments) {
    String args = arguments.trim();
    if (args.isEmpty()) {
      return new String[0];
    } else {
      return args.split("\\s+");
    }
  }

  private void addAfterSelectedArgument(String argument) {
    int selected = arguments.getSelectionModel().getSelectedIndex();
    if (selected >= 0 && selected < arguments.getItems().size()) {
      arguments.getItems().add(selected + 1, argument);
      arguments.getSelectionModel().select(selected + 1);
    } else {
      arguments.getItems().add(argument);
      arguments.getSelectionModel().select(arguments.getItems().size() - 1);
    }
  }
}
