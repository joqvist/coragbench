package drast.model;

import org.extendj.ast.CompilationUnit;

import java.io.File;

/**
 * Maps a source file to a parsed AST subtree for the
 * file.
 */
public class SourceFile {
  public final File file;
  public final CompilationUnit root;

  public SourceFile(File file, CompilationUnit root) {
    this.file = file;
    this.root = root;
  }

  @Override public String toString() {
    return "" + file;
  }
}
